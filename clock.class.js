class Clock {
  constructor() {
    this.seconds = 1;
    this.limit = 50;
  }

  go() {
    return new Promise((resolve) => {
      this.tic().then(() => {
        if(this.seconds < this.limit) {
          for(let i=0; i<2; i++) {
            this.go().then(() => {
              resolve();
            });
          }
        }

        if(this.seconds === this.limit) {
          resolve();
        }
      });
    });
  }

  simpleGo() {
    this.tic().then(() => {
      if(this.seconds < this.limit) {
        for(let i=0; i<2; i++) {
          this.simpleGo();
        }
      }

      console.log(this.seconds, this.limit);

      if(this.seconds === this.limit) {
        console.log(this.seconds, 'finish');
      }
    });
  }

  tic() {
    return new Promise((resolve) => {
      setTimeout(() => {
        console.log(this.seconds);
        this.seconds++;
        resolve();
      }, 500)
    });
  }
}

exports = module.exports = Clock;
